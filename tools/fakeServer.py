#!/usr/bin/env python3

from concurrent import futures
from threading import Event
import time
from FakeDataGenerator import FakeDataGenerator as FDG

import grpc
import grpc_bci_pb2 as bci
import grpc_bci_pb2_grpc as bci_grpc


class Client:
	def __init__(self, name):
		self.name = name
		self.samplePack = None
		self.sampleCount = 0
		self.samplePending = Event()

	def addSamplePack(self, samplePack):
		self.samplePack = samplePack
		self.sampleCount += 1
		self.samplePending.set()

	def popSamplePack(self):
		self.samplePending.wait()
		self.samplePending.clear()
		if self.sampleCount > 1:
			print("{0} missed sample packs: {1}".format(self.name, self.sampleCount - 1))
		self.sampleCount = 0
		return self.samplePack


class Server(bci_grpc.BCIServicer):
	def __init__(self, packSize):
		self.clientList = {}
		self.packSize = packSize

	def getSamplePack(self, request, context):
		if request.name not in self.clientList:
			print("new client: {0}".format(request.name))
			self.clientList[request.name] = Client(request.name)
		s = self.clientList[request.name].popSamplePack()
		return bci.SamplePack(samples = s)

	def addSamplePack(self, pack):
		sampleList = []
		for s in pack.T:
			sampleList.append(bci.Sample(values = s))
		for c in self.clientList.values():
			c.addSamplePack(sampleList)

class GRPCFakeServer:
	def __init__(self, channelCount, sampleRate, packSize):
		self.s = grpc.server(futures.ThreadPoolExecutor(max_workers = 10))
		self.server = Server(packSize)
		bci_grpc.add_BCIServicer_to_server(self.server, self.s)
		self.s.add_insecure_port('[::]:50051')
		self.s.start()

		self.channelCount = int(channelCount)
		self.sampleRate = float(sampleRate)
		self.packSize = int(packSize)

		self.fdg = FDG(channelCount, sampleRate, packSize)

	def run(self):
		print("\nGRPC Fake Server started")
		print("Channel count : {0:4}".format(self.channelCount))
		print("Sample rate   : {0:4} Hz".format(int(self.sampleRate)))
		print("Pack size     : {0:4}".format(self.packSize))
		print("Pack rate     : {0:4} Hz".format(int(self.sampleRate/self.packSize)))

		while True:
			self.server.addSamplePack(self.fdg.getSamplePack())
			time.sleep(1.0/(self.sampleRate/self.packSize))

if __name__ == '__main__':
	s = GRPCFakeServer(channelCount = 8, sampleRate = 250, packSize = 5)
	s.run()
