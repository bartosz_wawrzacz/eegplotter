import numpy as np

class FakeDataGenerator():
	def __init__(self, channelCount, sampleRateHz, samplePackSize,
		minFreqHz = 10, maxFreqHz = 20):
		super(FakeDataGenerator, self).__init__()
		self.channelCount = channelCount
		self.sampleRateHz = sampleRateHz
		self.samplePackLength = samplePackSize
		self.minFreqHz = minFreqHz
		self.maxFreqHz = maxFreqHz

		self.t = 0
		self.sweepMin = 1.0
		self.sweepMax = 50.0
		self.sweepStep = 0.02
		self.sweep = self.sweepMin

		# mi, ma = self.minFreqHz, self.maxFreqHz
		# self.freq = 2 * np.pi * (np.random.random((self.channelCount, 2)) * (ma-mi) + mi)
		self.freq = 2 * np.pi * np.array([[5, 5*(ch+1)] for ch in range(self.channelCount)])

		self.samples = np.zeros((self.channelCount, self.samplePackLength))

	def getSamplePack(self):
		for s in range(self.samplePackLength):
			self.t += 1/self.sampleRateHz
			self.sweep += self.sweepStep
			if self.sweep >= self.sweepMax:
				self.sweep = 0.0
				self.t = 0
			self.freq[0,1] = 2 * np.pi * self.sweep
			for ch in range(self.channelCount):
				d = 0.3 * np.sin(self.freq[ch][0] * self.t) \
				  + 0.8 * np.sin(self.freq[ch][1] * self.t) \
				  + 0.3 * (np.random.rand() - 0.5)
				self.samples[ch, s] = d

		return self.samples
