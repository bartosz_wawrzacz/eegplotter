#!/usr/bin/env python3

import time
import grpc
import grpc_bci_pb2 as bci
import grpc_bci_pb2_grpc as bci_grpc

channel = grpc.insecure_channel('localhost:50051')
stub = bci_grpc.BCIStub(channel)

while True:
	t = time.time()
	s = stub.getSamplePack(bci.ServiceId(name = "test"))
	print(time.time() - t)
	print(len(s.samples))
