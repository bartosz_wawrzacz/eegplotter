# README #

EEGPlotter will turn your meaningless EEG data samples into an equally vague bunch of plots!

Licensed under [WTFPL](http://www.wtfpl.net/)

## Dependencies ##

* Python3
* PyQt5 (https://www.riverbankcomputing.com/software/pyqt/download5)
```
apt install python3-pyqt5 python3-pyqt5.qtopengl
```
* numpy (http://www.numpy.org/)
```
pip3 install numpy
```
* VisPy (http://vispy.org/)
```
pip3 install vispy
```
* gRPC (https://grpc.io/)
```
pip3 install grpcio
```

## Usage ##

#### OpenBCI ####
The app is intended to be used as a visualization tool for `OpenBCI_Python`. The interface between these is provided by a plugin for the OpenBCI's `user.py` script.

To use it:

* copy the contents of `./OpenBCI_plugin` to your `OpenBCI_Python/plugins` folder
* run the `user.py` script with the plugin enabled:
```
./user.py -p /dev/your_tty --add grpc_server
```
* start the app:
```
./EEGPlotter.py
```

#### Tests / debugging ####

To run the app and make it fake some data on it's own, start it with the `fake` argument:
```
./EEGPlotter.py fake
```

To test the gRPC functionality using same fake data, first start the gRPC server:
```
tools/fakeServer.py
```
And then run the app as usual:
```
./EEGPlotter.py
```
A simple `serverTest.py` script is provided to test the gRPC server (be it the fake server or the `OpenBCI_Python` plugin).
