#/usr/bin/env sh

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. grpc_bci.proto
cp grpc_bci_pb2.py grpc_bci_pb2_grpc.py OpenBCI_plugin
cp grpc_bci_pb2.py grpc_bci_pb2_grpc.py tools