import numpy as np
from vispy import app
from vispy import gloo

class MultiChannelPlot(app.Canvas):
	vertex = """
	attribute float x;
	attribute float y;
	attribute float plotNum;	// TODO: should be an int, but fails to work as such
	
	uniform float maxY;

	uniform int plotLength;
	uniform int plotCount;
	uniform int selectedPlot;

	varying float vDrawLine;
	varying vec4 vColor;

	void main(void)
	{
		float scaledY = y / maxY;

		float clippedY;
		if(scaledY > 0.95)
			clippedY = 0.95;
		else if(scaledY < -0.95)
			clippedY = -0.95;
		else
			clippedY = scaledY;

		float py = clippedY/plotCount + 1 - 2*((plotNum * 2 + 1) / (plotCount * 2));
		gl_Position = vec4(x, py, 0.0, 1.0);

		if(x == -1 || x == 1 || clippedY == -0.95 || clippedY == 0.95)
			vDrawLine = 0;
		else
			vDrawLine = 1;

		if(plotNum == selectedPlot)
			vColor = vec4(0.5, 0.5, 1.0, 1.0);
		else
			vColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	"""

	fragment = """
	varying float vDrawLine;
	varying vec4 vColor;

	void main(void)
	{
		gl_FragColor = vColor;

		if(vDrawLine == 0)
			discard;
	}
	"""

	def __init__(self, plotCount, plotLength):
		super(MultiChannelPlot, self).__init__()
		gloo.set_clear_color('black')
		self.program = gloo.Program(self.vertex, self.fragment)
		
		self.plotLength = plotLength
		self.plotCount = plotCount
		self.plotNum = np.repeat(np.arange(self.plotCount), self.plotLength)
		self.selectedPlot = 0

		self.x = np.tile(np.linspace(-1, 1, self.plotLength), self.plotCount)
		self.y = np.zeros((self.plotCount, plotLength))

		self.maxY = 1.5

		self.program['x'] = self.x.astype(np.float32)
		self.program['y'] = self.y.astype(np.float32)
		self.program['maxY'] = self.maxY
		self.program['plotCount'] = self.plotCount
		self.program['plotNum'] = self.plotNum.astype(np.float32)
		self.program['selectedPlot'] = self.selectedPlot

	# this doesn't trigger every time, go figure >.<
	# resising is now handled in the on_draw() event handler
	# def on_resize(self, event):
	# 	gloo.set_viewport(0, 0, *event.size)

	def on_draw(self, event):
		gloo.set_viewport(0, 0, *self.physical_size)
		gloo.clear()
		self.program.draw('line_strip')

	def on_mouse_press(self, event):
		plot_height = self.physical_size[1] / self.plotCount
		self.selectedPlot = int(event.pos[1] / plot_height)
		self.updatePlot()

	def updatePlot(self, data=None):
		if data is not None:
			self.program['y'].set_data(data.astype(np.float32))
		self.program['selectedPlot'] = self.selectedPlot
		self.update()

	def setLimit(self, maxY):
		self.maxY = maxY
		self.program['maxY'] = self.maxY
