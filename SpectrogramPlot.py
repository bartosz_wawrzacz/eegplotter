import numpy as np
from vispy import app
from vispy import gloo

class SpectrogramPlot(app.Canvas):
	vertex = """
	attribute float x;
	attribute float y;
	attribute float a;

	varying float v_a;

	void main(void)
	{
		gl_Position = vec4(x, y, 0.0, 1.0);

		v_a = a / 20;
	}
	"""

	fragment = """
	varying float v_a;

	void main(void)
	{
		gl_FragColor = vec4(v_a, v_a, v_a, 1.0);
	}
	"""

	def __init__(self, sizeX, sizeY):
		super(SpectrogramPlot, self).__init__()
		gloo.set_clear_color('black')
		self.program = gloo.Program(self.vertex, self.fragment)

		self.sizeX = sizeX
		self.sizeY = sizeY
		
		x = np.repeat(np.arange(-1,1,2.0/sizeX), sizeY)
		y = np.tile(np.arange(-1,1,2.0/sizeY), sizeX)
		a = np.zeros(sizeX*sizeY)

		self.program['x'] = x.astype(np.float32)
		self.program['y'] = y.astype(np.float32)
		self.program['a'] = a.astype(np.float32)

	# this doesn't trigger every time, go figure >.<
	# resising is now handled in the on_draw() event handler
	# def on_resize(self, event):
	# 	gloo.set_viewport(0, 0, *event.size)

	def on_draw(self, event):
		gloo.set_viewport(0, 0, *self.physical_size)
		gloo.clear()
		self.program.draw('points')

	def updatePlot(self, data):
		self.program['a'].set_data(data.astype(np.float32))
		self.update()
