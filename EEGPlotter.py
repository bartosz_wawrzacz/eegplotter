#!/usr/bin/env python3

import numpy as np
import sys
if (sys.version_info > (3, 0)):
	from PyQt5 import QtCore
	from PyQt5 import QtWidgets as QtWhatever
else:
	from PyQt4 import QtCore
	from PyQt4 import QtGui as QtWhatever

from MultiChannelPlot import MultiChannelPlot
from FFTPlot import FFTPlot
from SimplePlot import SimplePlot
from SpectrogramPlot import SpectrogramPlot

from tools.FakeDataGenerator import FakeDataGenerator as FDG

import grpc
import grpc_bci_pb2 as bci
import grpc_bci_pb2_grpc as bci_grpc


class GrpcCollector(QtCore.QThread):
	dataReceived = QtCore.pyqtSignal(bci.SamplePack)

	def __init__(self):
		super(GrpcCollector, self).__init__()
		self.channel = grpc.insecure_channel('localhost:50051')
		self.stub = bci_grpc.BCIStub(self.channel)

	def run(self):
		while True:
			s = self.stub.getSamplePack(bci.ServiceId(name = "EEGPlotter"))
			self.dataReceived.emit(s)


class EEGPlotter(QtWhatever.QWidget):
	def __init__(self, channelCount, channelPointCount, fakeData=False, updateRate=0):
		super(EEGPlotter, self).__init__()
		self.updateRate = updateRate
		self.layout = QtWhatever.QGridLayout()
		self.channelCount = channelCount
		self.channelPointCount = channelPointCount
		self.fftPointCount = 100

		self.plotter = MultiChannelPlot(self.channelCount, self.channelPointCount)
		self.plotter.setLimit(1.0)
		self.fft = FFTPlot(self.fftPointCount)
		self.spec = SpectrogramPlot(channelPointCount, self.fftPointCount)

		self.layout.addWidget(self.plotter.native, 0, 0, 2, 1)
		self.layout.addWidget(self.fft.native, 0, 1)
		self.layout.addWidget(self.spec.native, 1, 1)

		self.setLayout(self.layout)
		self.showMaximized()

		self.data = np.zeros((self.channelCount, self.channelPointCount))
		self.fftData = np.zeros((self.channelCount, self.channelPointCount, self.fftPointCount))

		if not fakeData:
			self.collector = GrpcCollector()
			self.collector.dataReceived.connect(self.addSamplePack)
			self.collector.start()
		else:
			self.fakeSampleRateHz = 250.0
			self.fakeSamplePackLength = 5
			self.fdg = FDG(self.channelCount, self.fakeSampleRateHz, self.fakeSamplePackLength, maxFreqHz = 50)
			self.fakeDataTimer = QtCore.QTimer()
			self.fakeDataTimer.timeout.connect(self.addFakeSamplePack)
			self.fakeDataTimer.start(1000/(self.fakeSampleRateHz/self.fakeSamplePackLength))

		if updateRate != 0:
			self.plotUpdateTimer = QtCore.QTimer()
			self.plotUpdateTimer.timeout.connect(self.updatePlots)
			self.plotUpdateTimer.start(1000.0/updateRate)

	def addSamplePack(self, pack):
		l = len(pack.samples)
		self.data[:, :-l] = self.data[:, l:]
		self.fftData[:, :-l, :] = self.fftData[:, l:, :]

		for i in range(l):
			self.data[:, -l+i] = pack.samples[i].values

			for ch in range(self.channelCount):
				data = self.data[ch, -self.fftPointCount*2-l+i:-l+i]
				wnd = np.blackman(self.fftPointCount*2)
				fft = np.fft.rfft(data * wnd)
				fft_abs = np.absolute(fft)
				self.fftData[ch,-l+i] = fft_abs[:self.fftPointCount]

		if self.updateRate == 0:
			self.updatePlots()

	def addFakeSamplePack(self):
		pack = self.fdg.getSamplePack()
		sampleList = []
		for s in pack.T:
			sampleList.append(bci.Sample(values = s))
		self.addSamplePack(bci.SamplePack(samples = sampleList))

	def updatePlots(self):
		self.plotter.updatePlot(self.data)
		self.fft.updatePlot(self.data[self.plotter.selectedPlot][-self.fftPointCount*2:])
		self.spec.updatePlot(self.fftData[self.plotter.selectedPlot])


if __name__ == '__main__':
	app = QtWhatever.QApplication(sys.argv)
	fake = True if 'fake' in sys.argv else False
	plt = EEGPlotter(8, 1024, fakeData=fake)
	app.exec_()
