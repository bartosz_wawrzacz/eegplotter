import numpy as np
from vispy import app
from vispy import gloo

class SimplePlot(app.Canvas):
	vertex = """
	attribute float x;
	attribute float y;
	
	void main(void)
	{
		gl_Position = vec4(x, y, 0.0, 1.0);
	}
	"""

	fragment = """
	void main(void)
	{
		gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	"""

	def __init__(self):
		super(SimplePlot, self).__init__()
		gloo.set_clear_color('black')
		self.program = gloo.Program(self.vertex, self.fragment)
		
		x = np.zeros(10)
		y = np.zeros(10)

		self.program['x'] = x.astype(np.float32)
		self.program['y'] = y.astype(np.float32)

	# this doesn't trigger every time, go figure >.<
	# resising is now handled in the on_draw() event handler
	# def on_resize(self, event):
	# 	gloo.set_viewport(0, 0, *event.size)

	def on_draw(self, event):
		gloo.set_viewport(0, 0, *self.physical_size)
		gloo.clear()
		self.program.draw('line_strip')

	def updatePlot(self, data):
		x = np.linspace(-1, 1, len(data))
		self.program['x'].set_data(x.astype(np.float32))
		self.program['y'].set_data(data.astype(np.float32))
		self.update()
