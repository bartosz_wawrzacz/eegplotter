import numpy as np
from vispy import app
from vispy import gloo

class FFTPlot(app.Canvas):
	vertex = """
	attribute float x;
	attribute float y;
	
	uniform float maxY;

	void main(void)
	{
		float yy = y/maxY * 1.8 - 0.9;
		gl_Position = vec4(x, yy, 0.0, 1.0);
	}
	"""

	fragment = """
	void main(void)
	{
		gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
	}
	"""

	def __init__(self, plotLength):
		super(FFTPlot, self).__init__()
		gloo.set_clear_color('black')
		self.program = gloo.Program(self.vertex, self.fragment)
		
		self.plotLength = plotLength

		self.x = np.linspace(-1, 1, self.plotLength)
		self.y = np.zeros(self.plotLength)

		self.program['x'] = self.x.astype(np.float32)
		self.program['y'] = self.y.astype(np.float32)
		self.program['maxY'] = 0

	# this doesn't trigger every time, go figure >.<
	# resising is now handled in the on_draw() event handler
	# def on_resize(self, event):
	# 	gloo.set_viewport(0, 0, *event.size)

	def on_draw(self, event):
		gloo.set_viewport(0, 0, *self.physical_size)
		gloo.clear()
		self.program.draw('line_strip')

	def updatePlot(self, data):
		wnd = np.blackman(len(data))
		fft = np.fft.rfft(data * wnd)
		fft_abs = np.absolute(fft)
		fft_plot = fft_abs[:self.plotLength]
		self.program['y'].set_data(fft_plot.astype(np.float32))
		# self.program['maxY'] = fft_plot.max()
		self.program['maxY'] = self.plotLength / 5.0
		self.update()
