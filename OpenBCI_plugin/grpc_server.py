import plugin_interface as plugintypes
from concurrent import futures
from threading import Event
import sys

sys.path.append("~/OpenBCI_Python/plugins") # please fix me
import grpc
import grpc_bci_pb2 as bci
import grpc_bci_pb2_grpc as bci_grpc


class Client:
	def __init__(self, name):
		self.name = name
		self.samplePack = None
		self.sampleCount = 0
		self.samplePending = Event()

	def AddSamplePack(self, samplePack):
		self.samplePack = samplePack
		self.sampleCount += 1
		self.samplePending.set()

	def PopSamplePack(self):
		self.samplePending.wait()
		self.samplePending.clear()
		if self.sampleCount > 1:
			print("{0} missed sample packs: {1}".format(self.name, self.sampleCount - 1))
		self.sampleCount = 0
		return self.samplePack


class Server(bci_grpc.BCIServicer):
	def __init__(self, packSize):
		self.clientList = {}
		self.sampleList = []
		self.packSize = packSize

	def GetSample(self, request, context):
		if request.name not in self.clientList:
			print("new client: {0}".format(request.name))
			self.clientList[request.name] = Client(request.name)
		s = self.clientList[request.name].PopSamplePack()
		return bci.SamplePack(samples = s)

	def AddNewSample(self, sample):
		self.sampleList.append(bci.Sample(values = sample))
		if len(self.sampleList) == self.packSize:
			for c in self.clientList.values():
				c.AddSamplePack(self.sampleList)
			self.sampleList = []


class GRPCServer(plugintypes.IPluginExtended):
	def activate(self):
		self.s = grpc.server(futures.ThreadPoolExecutor(max_workers = 10))
		self.server = Server(packSize = 5)
		bci_grpc.add_BCIServicer_to_server(self.server, self.s)
		self.s.add_insecure_port('[::]:50051')
		self.s.start()

		print(">>>>>> GRPC Server started")
	
	# called with each new sample
	def __call__(self, sample):
		if sample:
			self.server.AddNewSample(sample.channel_data)
